/**
*
* A semantic error usually happens at run time (semantics is meaning of code). 
* It is an error in application logic that can cause
* application to produce result that is not correct. Alternatively semantic error can cause application
* to crash.
*
* EXAMPLE
*
* read couple of integers from command line and try division, the application is going to fail in case of
* division by zero
*
* EXAMPLE ERROR MESSAGE
*
* Exception in thread "main" java.lang.ArithmeticException: / by zero
*   at SemtError.main(SemtErr.java:33)
*
* COMPILE INSTRUCTIONS
*
* javac SemtErr.java
*
*/
class SemtErr{
    public static void main(String[] args){
        int a,b,c;
        if(args.length != 2){
            System.out.println("Usage java SemtError a b");
            System.exit(1);
        }
        a=Integer.parseInt(args[0]);
        b=Integer.parseInt(args[1]);
        c=a/b;
        System.out.println(c);
    }
}
