/**
*
* A syntax error is reported if language or compiler rules are broken (usually by the programmer).
* In such a case a compiler usually reports an error that needs to be fixed. 
* In this example a single dot is missing, and compile error message is printed.
*
* ERROR MESSAGE
*
* SyntaxErr.java:13: error: not a statement
*        System.out,println("An example of syntax error");
*              ^
* SyntaxErr.java:13: error: ';' expected
*        System.out,println("An example of syntax error");
*                  ^
* 2 errors
*
*
* COMPILE INSTRUCTIONS
*
* javac SyntaxError.java
*
*/
class SyntaxErr{
    public static void main(String[] args){
        System.out,println("An example of syntax error");
    }
}
